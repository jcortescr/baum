<?php
/**
 *
 * @link              https://jonathancortes.net
 * @since             1.0.0
 * @package           Calacas
 *
 * @wordpress-plugin
 * Plugin Name:       Producciones Calacas
 * Plugin URI:        https://jonathancortes.net
 * Description:       This is a Festival Management plugin.
 * Version:           1.0.0
 * Author:            Jonathan Cortes
 * Author URI:        https://jonathancortes.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       calacas
 */

//Register a custom menu page.
function festival_menu() {
    add_menu_page ( 'Festivals', 'Festivals', 'manage_options', 'edit.php?post_type=festival', '', 'dashicons-buddicons-groups', 5);
    add_submenu_page( 'edit.php?post_type=festival', 'Events', 'Events', 'manage_options', 'edit.php?post_type=event', '');
    add_submenu_page( 'edit.php?post_type=festival', 'Artist', 'Artists', 'manage_options', 'edit.php?post_type=artist', '');
}
add_action( 'admin_menu', 'festival_menu');

//Helper
require plugin_dir_path( __FILE__ ) . 'includes/helper.php';

//Call to custom post types
require plugin_dir_path( __FILE__ ) . 'includes/festival-cpt.php';
require plugin_dir_path( __FILE__ ) . 'includes/event-cpt.php';
require plugin_dir_path( __FILE__ ) . 'includes/artist-cpt.php';

//REST API
require plugin_dir_path( __FILE__ ) . 'includes/rest_api.php';
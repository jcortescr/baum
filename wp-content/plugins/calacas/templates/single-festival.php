<?php
/**
 * The template for displaying single festival
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
?>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();
			?>
			<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

			<?php
		
			get_template_part( 'template-parts/entry-header' );
		
			if ( ! is_search() ) {
				get_template_part( 'template-parts/featured-image' );
			}
		
			?>
		
			<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
		
				<div class="entry-content">

					<img src="<?php echo festival_get_meta( 'festival_information_logo' ); ?>" alt="" width="300px">
					<label for=""><b>Location:</b> <?php echo festival_get_meta( 'festival_information_location' ); ?></label>
					<label for=""><b>Start Date:</b> <?php echo date("m/d/Y", strtotime(festival_get_meta( 'festival_information_start_date' ))); ?></label>
					<label for=""><b>End Date:</b> <?php echo date("m/d/Y", strtotime(festival_get_meta( 'festival_information_end_date' ))); ?></label>
					<label for=""><b>Description:</b></label>
					<?php
					if ( is_search() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
						the_excerpt();
					} else {
						the_content( __( 'Continue reading', 'twentytwenty' ) );
					}
					?>
					<?php $events = getEventsByFestival(get_the_ID()); ?>
					<div class="events">
						<h3>Events:</h3>
						<?php foreach ( $events as $event ) {
							echo '<h5>'.festival_get_meta( 'event_information_event_name', $event->ID ).'</h5>';
							echo wpautop($event->post_content);
							$artists = unserialize(festival_get_meta( 'event_information_artists', $event->ID ));
							echo '<h6>Artists:</h6>';
							echo '<ul>';
							foreach( $artists as $artist_ID ){
								echo '<li>'.get_the_title($artist_ID).'</li>';
							}
							echo '</ul>';
						} ?>
					</div>
		
				</div><!-- .entry-content -->
		
			</div><!-- .post-inner -->
		
			<div class="section-inner">
				<?php
				wp_link_pages(
					array(
						'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'twentytwenty' ) . '"><span class="label">' . __( 'Pages:', 'twentytwenty' ) . '</span>',
						'after'       => '</nav>',
						'link_before' => '<span class="page-number">',
						'link_after'  => '</span>',
					)
				);
		
				edit_post_link();
		
				// Single bottom post meta.
				twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );
		
				if ( is_single() ) {
		
					get_template_part( 'template-parts/entry-author-bio' );
		
				}
				?>
		
			</div><!-- .section-inner -->
		
			<?php
		
			if ( is_single() ) {
		
				get_template_part( 'template-parts/navigation' );
		
			}
		
			/**
			 *  Output comments wrapper if it's a post, or if comments are open,
			 * or if there's a comment number – and check for password.
			 * */
			if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
				?>
		
				<div class="comments-wrapper section-inner">
		
					<?php comments_template(); ?>
		
				</div><!-- .comments-wrapper -->
		
				<?php
			}
			?>
		
		</article><!-- .post -->
		<?php
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
<?php
// Register Custom Post Type

function artist_cpt() {

	$labels = array(
		'name'                  => _x( 'Artists', 'Post Type General Name', 'calacas' ),
		'singular_name'         => _x( 'Artist', 'Post Type Singular Name', 'calacas' ),
		'menu_name'             => __( 'Artists', 'calacas' ),
		'name_admin_bar'        => __( 'Artist', 'calacas' ),
		'archives'              => __( 'Item Archives', 'calacas' ),
		'attributes'            => __( 'Item Attributes', 'calacas' ),
		'parent_item_colon'     => __( 'Parent Item:', 'calacas' ),
		'all_items'             => __( 'All Items', 'calacas' ),
		'add_new_item'          => __( 'Add New Item', 'calacas' ),
		'add_new'               => __( 'Add New', 'calacas' ),
		'new_item'              => __( 'New Item', 'calacas' ),
		'edit_item'             => __( 'Edit Item', 'calacas' ),
		'update_item'           => __( 'Update Item', 'calacas' ),
		'view_item'             => __( 'View Item', 'calacas' ),
		'view_items'            => __( 'View Items', 'calacas' ),
		'search_items'          => __( 'Search Item', 'calacas' ),
		'not_found'             => __( 'Not found', 'calacas' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'calacas' ),
		'featured_image'        => __( 'Featured Image', 'calacas' ),
		'set_featured_image'    => __( 'Set featured image', 'calacas' ),
		'remove_featured_image' => __( 'Remove featured image', 'calacas' ),
		'use_featured_image'    => __( 'Use as featured image', 'calacas' ),
		'insert_into_item'      => __( 'Insert into item', 'calacas' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'calacas' ),
		'items_list'            => __( 'Items list', 'calacas' ),
		'items_list_navigation' => __( 'Items list navigation', 'calacas' ),
		'filter_items_list'     => __( 'Filter items list', 'calacas' ),
	);
	$args = array(
		'label'                 => __( 'Artist', 'calacas' ),
		'description'           => __( 'Artist Management', 'calacas' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-audio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'artist', $args );

}
add_action( 'init', 'artist_cpt', 0 );

/**
 * Custom fields
 */

function artist_information_add_meta_box() {
	add_meta_box(
		'artist_information-artist-information',
		__( 'Artist Information', 'artist_information' ),
		'artist_information_html',
		'artist',
		'side',
		'default'
	);
}
add_action( 'add_meta_boxes', 'artist_information_add_meta_box' );

function artist_information_html( $post) {
	wp_nonce_field( '_artist_information_nonce', 'artist_information_nonce' ); ?>

	<p>
		<label for="artist_information_email"><?php _e( 'Email', 'artist_information' ); ?></label><br>
		<input type="text" name="artist_information_email" id="artist_information_email" value="<?php echo festival_get_meta( 'artist_information_email' ); ?>">
	</p>	<p>
		<label for="artist_information_phone"><?php _e( 'Phone', 'artist_information' ); ?></label><br>
		<input type="text" name="artist_information_phone" id="artist_information_phone" value="<?php echo festival_get_meta( 'artist_information_phone' ); ?>">
	</p>	<p>
		<label for="artist_information_additional_information"><?php _e( 'Additional Information', 'artist_information' ); ?></label><br>
		<textarea name="artist_information_additional_information" id="artist_information_additional_information" cols="35" rows="3"><?php echo festival_get_meta( 'artist_information_additional_information' ); ?></textarea>
	</p><?php
}

function artist_information_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['artist_information_nonce'] ) || ! wp_verify_nonce( $_POST['artist_information_nonce'], '_artist_information_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['artist_information_email'] ) )
		update_post_meta( $post_id, 'artist_information_email', esc_attr( $_POST['artist_information_email'] ) );
	if ( isset( $_POST['artist_information_phone'] ) )
		update_post_meta( $post_id, 'artist_information_phone', esc_attr( $_POST['artist_information_phone'] ) );
	if ( isset( $_POST['artist_information_additional_information'] ) )
		update_post_meta( $post_id, 'artist_information_additional_information', esc_attr( $_POST['artist_information_additional_information'] ) );
}
add_action( 'save_post', 'artist_information_save' );

/*
	Usage: festival_get_meta( 'artist_information_email' )
	Usage: festival_get_meta( 'artist_information_phone' )
	Usage: festival_get_meta( 'artist_information_additional_information' )
*/

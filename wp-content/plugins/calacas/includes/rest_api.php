<?php 
//REST API to show all artists
add_action( 'rest_api_init', function () {
    register_rest_route( 'calacas/v1', '/artists(?:/event/(?P<event_id>\d+))?', array(
        'methods' => 'GET',
        'callback' => 'ep_get_festival_artists',
        'args' => array(
            'event_id' => array(
                'validate_callback' => function($param, $request, $key) {
                    return is_numeric( $param );
                }
            ),
        ),
    ) );
} );
  
//Callback
function ep_get_festival_artists( $data ) {
    global $wpdb;
    $event_id = $data['event_id'];

    if($event_id){
        $artists = unserialize(festival_get_meta( 'event_information_artists', $event_id ));
    }else{
        $artists = get_posts(array(
            'fields'      => 'ids',
            'numberposts' => -1,
            'post_type'   => 'artist',
            'post_status' => 'publish'
        ));             
    }
    $result = getArtistInformation($artists);

    return $result;
}

function getArtistInformation($artists){
    $artist_info = array();
    $i = 0;
    foreach( $artists as $artist_ID ){
        $artist_info[$i]['name'] = get_the_title($artist_ID);
        $artist_info[$i]['email'] = festival_get_meta( 'artist_information_email',$artist_ID);
        $artist_info[$i]['phone'] = festival_get_meta( 'artist_information_phone',$artist_ID);
        $artist_info[$i]['additional_information'] = festival_get_meta( 'artist_information_additional_information',$artist_ID);
        $i++;
    }
    return $artist_info;
}
<?php
// Register Custom Post Type

function festival_cpt() {

	$labels = array(
		'name'                  => _x( 'Festivals', 'Post Type General Name', 'calacas' ),
		'singular_name'         => _x( 'Festival', 'Post Type Singular Name', 'calacas' ),
		'menu_name'             => __( 'Festivals', 'calacas' ),
		'name_admin_bar'        => __( 'Festival', 'calacas' ),
		'archives'              => __( 'Item Archives', 'calacas' ),
		'attributes'            => __( 'Item Attributes', 'calacas' ),
		'parent_item_colon'     => __( 'Parent Item:', 'calacas' ),
		'all_items'             => __( 'All Items', 'calacas' ),
		'add_new_item'          => __( 'Add New Item', 'calacas' ),
		'add_new'               => __( 'Add New', 'calacas' ),
		'new_item'              => __( 'New Item', 'calacas' ),
		'edit_item'             => __( 'Edit Item', 'calacas' ),
		'update_item'           => __( 'Update Item', 'calacas' ),
		'view_item'             => __( 'View Item', 'calacas' ),
		'view_items'            => __( 'View Items', 'calacas' ),
		'search_items'          => __( 'Search Item', 'calacas' ),
		'not_found'             => __( 'Not found', 'calacas' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'calacas' ),
		'featured_image'        => __( 'Featured Image', 'calacas' ),
		'set_featured_image'    => __( 'Set featured image', 'calacas' ),
		'remove_featured_image' => __( 'Remove featured image', 'calacas' ),
		'use_featured_image'    => __( 'Use as featured image', 'calacas' ),
		'insert_into_item'      => __( 'Insert into item', 'calacas' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'calacas' ),
		'items_list'            => __( 'Items list', 'calacas' ),
		'items_list_navigation' => __( 'Items list navigation', 'calacas' ),
		'filter_items_list'     => __( 'Filter items list', 'calacas' ),
	);
	$args = array(
		'label'                 => __( 'Festival', 'calacas' ),
		'description'           => __( 'Festival Management', 'calacas' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-buddicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'festival', $args );

}
add_action( 'init', 'festival_cpt', 0 );

/**
 * Custom fields
 */

function festival_information_add_meta_box() {
	add_meta_box(
		'festival_information-festival-information',
		__( 'Festival Information', 'festival_information' ),
		'festival_information_html',
		'festival',
		'side',
		'default'
	);
}
add_action( 'add_meta_boxes', 'festival_information_add_meta_box' );

function festival_information_html( $post) {
	wp_nonce_field( '_festival_information_nonce', 'festival_information_nonce' ); ?>

	<p>
		<label for="festival_information_logo"><?php _e( 'Logo', 'festival_information' ); ?></label><br>
		<img src="<?php echo festival_get_meta( 'festival_information_logo' ); ?>" id="festival_information_logo_img" style="width:100%">
		<input type="text" name="festival_information_logo" id="festival_information_logo" value="<?php echo festival_get_meta( 'festival_information_logo' ); ?>">
		<input class="button open-media-box" id="festival_information_logo_button" name="festival_information_logo_button" type="button" value="Upload" />
	</p><p>
		<label for="festival_information_location"><?php _e( 'Location', 'festival_information' ); ?></label><br>
		<input type="text" name="festival_information_location" id="festival_information_location" value="<?php echo festival_get_meta( 'festival_information_location' ); ?>">
	</p><p>
		<label for="festival_information_start_date"><?php _e( 'Start Date', 'festival_information' ); ?></label><br>
		<input type="date" name="festival_information_start_date" id="festival_information_start_date" value="<?php echo festival_get_meta( 'festival_information_start_date' ); ?>">
	</p><p>
		<label for="festival_information_end_date"><?php _e( 'End Date', 'festival_information' ); ?></label><br>
		<input type="date" name="festival_information_end_date" id="festival_information_end_date" value="<?php echo festival_get_meta( 'festival_information_end_date' ); ?>">
	</p><?php
}

function festival_information_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['festival_information_nonce'] ) || ! wp_verify_nonce( $_POST['festival_information_nonce'], '_festival_information_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['festival_information_logo'] ) )
		update_post_meta( $post_id, 'festival_information_logo', esc_attr( $_POST['festival_information_logo'] ) );
	if ( isset( $_POST['festival_information_location'] ) )
		update_post_meta( $post_id, 'festival_information_location', esc_attr( $_POST['festival_information_location'] ) );
	if ( isset( $_POST['festival_information_start_date'] ) )
		update_post_meta( $post_id, 'festival_information_start_date', esc_attr( $_POST['festival_information_start_date'] ) );
	if ( isset( $_POST['festival_information_end_date'] ) )
		update_post_meta( $post_id, 'festival_information_end_date', esc_attr( $_POST['festival_information_end_date'] ) );
}
add_action( 'save_post', 'festival_information_save' );

/*
	Usage: festival_get_meta( 'festival_information_logo' )
	Usage: festival_get_meta( 'festival_information_location' )
	Usage: festival_get_meta( 'festival_information_start_date' )
	Usage: festival_get_meta( 'festival_information_end_date' )
*/

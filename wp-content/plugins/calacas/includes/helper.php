<?php 
//This are functions used by the plugin

//Get festivals, return object
function getFestivals(){
    $args = array(
        'numberposts' => -1,
        'post_type'   => 'festival',
        'post_status' => 'publish'
    );
       
    return get_posts( $args );
}

//Get events, return object
function getEvents(){
    $args = array(
        'numberposts' => -1,
        'post_type'   => 'event',
        'post_status' => 'publish'
    );
       
    return get_posts( $args );
}

//Get artists, return object
function getArtists(){
    $args = array(
        'numberposts' => -1,
        'post_type'   => 'artist',
        'post_status' => 'publish'
    );
       
    return get_posts( $args );
}

//Get events by festival, param festival ID, return object
function getEventsByFestival($id){
    $args = array(
        'numberposts' => -1,
        'post_type'   => 'event',
        'post_status' => 'publish',
        'meta_query'  => array(
            array(
                'key'     => 'event_information_festival',
                'value'   => $id,
                'compare' => '=',
            )
        )
    );
       
    return get_posts( $args );  
}

//Get post meta
function festival_get_meta( $value, $id = '' ) {
    global $post;
    
	$post_id = ($id) ? $id : $post->ID;

	$field = get_post_meta( $post_id, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}

//This add a js script to open the media box, it allow to upload/save an image
function custom_media_image_upload() {
    ?><script>
        jQuery(document).ready(function($){
            if ( typeof wp.media !== 'undefined' ) {
                var _custom_media = true,
                _orig_send_attachment = wp.media.editor.send.attachment;
                $('.open-media-box').click(function(e) {
                    var send_attachment_bkp = wp.media.editor.send.attachment;
                    var button = $(this);
                    var id = button.attr('id').replace('_button', '');
                    _custom_media = true;
                        wp.media.editor.send.attachment = function(props, attachment){
                        if ( _custom_media ) {
                            $("#"+id).val(attachment.url);
                            $("#"+id+"_img").attr('src',attachment.url);
                        } else {
                            return _orig_send_attachment.apply( this, [props, attachment] );
                        };
                    }
                    wp.media.editor.open(button);

                    return false;
                });
                $('.add_media').on('click', function(){
                    _custom_media = false;
                });
            }
        });
    </script><?php
}
add_action( 'admin_footer', 'custom_media_image_upload' );

//This override the festival archive template
function use_festival_template($tpl){
    if ( is_singular ( 'festival' ) ) {
      $tpl = plugin_dir_path(__DIR__) . '/templates/single-festival.php';
    }
    return $tpl;
}
add_filter( 'single_template', 'use_festival_template' ) ;
<?php
// Register Custom Post Type

function event_cpt() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'calacas' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'calacas' ),
		'menu_name'             => __( 'Events', 'calacas' ),
		'name_admin_bar'        => __( 'Event', 'calacas' ),
		'archives'              => __( 'Item Archives', 'calacas' ),
		'attributes'            => __( 'Item Attributes', 'calacas' ),
		'parent_item_colon'     => __( 'Parent Item:', 'calacas' ),
		'all_items'             => __( 'All Items', 'calacas' ),
		'add_new_item'          => __( 'Add New Item', 'calacas' ),
		'add_new'               => __( 'Add New', 'calacas' ),
		'new_item'              => __( 'New Item', 'calacas' ),
		'edit_item'             => __( 'Edit Item', 'calacas' ),
		'update_item'           => __( 'Update Item', 'calacas' ),
		'view_item'             => __( 'View Item', 'calacas' ),
		'view_items'            => __( 'View Items', 'calacas' ),
		'search_items'          => __( 'Search Item', 'calacas' ),
		'not_found'             => __( 'Not found', 'calacas' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'calacas' ),
		'featured_image'        => __( 'Featured Image', 'calacas' ),
		'set_featured_image'    => __( 'Set featured image', 'calacas' ),
		'remove_featured_image' => __( 'Remove featured image', 'calacas' ),
		'use_featured_image'    => __( 'Use as featured image', 'calacas' ),
		'insert_into_item'      => __( 'Insert into item', 'calacas' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'calacas' ),
		'items_list'            => __( 'Items list', 'calacas' ),
		'items_list_navigation' => __( 'Items list navigation', 'calacas' ),
		'filter_items_list'     => __( 'Filter items list', 'calacas' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'calacas' ),
		'description'           => __( 'Event Management', 'calacas' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'event_cpt', 0 );

/**
 * Custom fields
 */
function event_information_add_meta_box() {
	add_meta_box(
		'event_information-event-information',
		__( 'Event Information', 'event_information' ),
		'event_information_html',
		'event',
		'side',
		'default'
	);
}
add_action( 'add_meta_boxes', 'event_information_add_meta_box' );

function event_information_html( $post) {
	wp_nonce_field( '_event_information_nonce', 'event_information_nonce' ); ?>

	<p>
		<label for="event_information_event_date"><?php _e( 'Event Name', 'event_information' ); ?></label><br>
		<input type="text" name="event_information_event_name" id="event_information_event_name" value="<?php echo festival_get_meta( 'event_information_event_name' ); ?>">
    </p><p>	
		<label for="event_information_event_date"><?php _e( 'Event Date', 'event_information' ); ?></label><br>
		<input type="date" name="event_information_event_date" id="event_information_event_date" value="<?php echo festival_get_meta( 'event_information_event_date' ); ?>">
    </p><p>	
        <label for="event_information_start_hour"><?php _e( 'Start Hour', 'event_information' ); ?></label><br>
		<input type="time" name="event_information_start_hour" id="event_information_start_hour" value="<?php echo festival_get_meta( 'event_information_start_hour' ); ?>">
    </p><p>
        <label for="event_information_end_hour"><?php _e( 'End Hour', 'event_information' ); ?></label><br>
		<input type="time" name="event_information_end_hour" id="event_information_end_hour" value="<?php echo festival_get_meta( 'event_information_end_hour' ); ?>">
    </p><p>
		<label for="event_information_festival"><?php _e( 'Festival', 'event_information' ); ?></label><br>
        <?php $festivals = getFestivals(); ?>
        <select name="event_information_festival" id="event_information_festival">
            <option value="">Festival Selection</option>
            <?php foreach( $festivals as $festival ) : ?>
                <option value="<?php echo $festival->ID; ?>" <?php echo (festival_get_meta( 'event_information_festival' ) == $festival->ID ) ? 'selected' : '' ?>><?php echo $festival->post_title; ?></option>
            <?php endforeach; ?>
        </select>
    </p><p>	
        <label for="event_information_artists"><?php _e( 'Artists', 'event_information' ); ?></label><br>
		<?php 

		$artists = getArtists(); 
		$saved_artists = festival_get_meta('event_information_artists') ? unserialize(festival_get_meta( 'event_information_artists' )) : array() ;
		
		foreach( $artists as $artist ) : ?>
			<input type="checkbox" name="event_information_artists[]" id="event_artist_<?php echo $artist->ID; ?>" value="<?php echo $artist->ID; ?>" <?php echo in_array( $artist->ID, $saved_artists) ? 'checked' : '' ?>>
			<label for="event_artist_<?php echo $artist->ID; ?>"><?php echo $artist->post_title; ?></label><br>      
        <?php endforeach; ?>
    </p>
    
	<?php
}

function event_information_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['event_information_nonce'] ) || ! wp_verify_nonce( $_POST['event_information_nonce'], '_event_information_nonce' ) ) return;
	if ( ! current_user_can( 'edit_post', $post_id ) ) return;

	if ( isset( $_POST['event_information_event_name'] ) )
		update_post_meta( $post_id, 'event_information_event_name', esc_attr( $_POST['event_information_event_name'] ) );
	if ( isset( $_POST['event_information_event_date'] ) )
		update_post_meta( $post_id, 'event_information_event_date', esc_attr( $_POST['event_information_event_date'] ) );
	if ( isset( $_POST['event_information_start_hour'] ) )
        update_post_meta( $post_id, 'event_information_start_hour', esc_attr( $_POST['event_information_start_hour'] ) );
    if ( isset( $_POST['event_information_end_hour'] ) )
		update_post_meta( $post_id, 'event_information_end_hour', esc_attr( $_POST['event_information_end_hour'] ) );
	if ( isset( $_POST['event_information_festival'] ) )
		update_post_meta( $post_id, 'event_information_festival', esc_attr( $_POST['event_information_festival'] ) );
	if ( isset( $_POST['event_information_artists'] ) )
		update_post_meta( $post_id, 'event_information_artists', serialize($_POST['event_information_artists']) );
}
add_action( 'save_post', 'event_information_save' );

/*
	Usage: festival_get_meta( 'event_information_event_name' )
	Usage: festival_get_meta( 'event_information_event_date' )
	Usage: festival_get_meta( 'event_information_start_hour' )
	Usage: festival_get_meta( 'event_information_festival' )
	Usage: festival_get_meta( 'event_information_artists' )
*/

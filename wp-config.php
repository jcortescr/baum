<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define( 'DB_NAME', 'baum' );

/** Tu nombre de usuario de MySQL */
define( 'DB_USER', 'root' );

/** Tu contraseña de MySQL */
define( 'DB_PASSWORD', '' );

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define( 'DB_HOST', 'localhost' );

/** Codificación de caracteres para la base de datos. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '0V{:dS#L53s }I;_7m@XL^`%`OQlA?]b_7#K|;)%q)RrE{u//8!{eza}^I*Zk>^`' );
define( 'SECURE_AUTH_KEY', '3?x?.rr2~|B&u)KUq:<]_hqL@+~B$+acG~j22FV~}U}HqZK=JME+b&qRgI]o#j=z' );
define( 'LOGGED_IN_KEY', '[{3U4Z*@$7k2|TnJM#AY/I5F<&JQa9?+E;`?TKChn,262;]G_p/4]9u$ATQ]XrTF' );
define( 'NONCE_KEY', 's%idAEfv/lgZs^{HS=7g1]$YNmDcN:]NJ{Z&>77oV6&(0G7XgGyqmpvdl4J6[Rz6' );
define( 'AUTH_SALT', 'Vm[(mCd3yiSj@d@OIj^Mz#[KIDbs{D~~c:=.M}7XA+^`T2tL(#v0!Kw75zn3Ioge' );
define( 'SECURE_AUTH_SALT', 'Mk(T1:|L;>M2h[=0BV;Q)EC4#r@Ea7<WFHCEPgO]c%75Ht<HNuLm:Ej!D#)yL}N)' );
define( 'LOGGED_IN_SALT', '|u Y5&~! 0FL5Y2W4=N.E;mx>u3/Tj@Ri8.{q#Cx7,U&k3KRKtTd_S[IfS <h(`8' );
define( 'NONCE_SALT', '$hFA7-C-?/i}&O}itZO~GI9.%!>Lw[Zt@rL`Rkk5M nA{+uoM2d2ILn=r.Ks1DQ=' );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'baum_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

